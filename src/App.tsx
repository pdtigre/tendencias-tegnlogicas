import React from 'react';
import { Layout, Menu, Breadcrumb } from 'antd';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import SiderMenu from './components/Layout';

const { Header, Content, Footer } = Layout;

const routes = [
  { path: '/', label: 'Inicio'} ,
  { path: '/ptigre', label: 'Paola Tigre'},
  { path: '/ppulgarin', label: 'Pedro Pulgarin'},
  { path: '/dgaray', label: 'David Garay'},
  { path: '/sfernandez', label: 'Steven Fernandez'},
  { path: '/fheredia', label: 'Francisco Heredia'},
]

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Layout className='layout'>
        <SiderMenu menuItems={routes} />
        <Layout.Content>
          <Routes>
            <Route path="/" />
            <Route path="/ptigre"/>
            <Route path="/ppulgarin" />
            <Route path="/dgaray" />
            <Route path="/sfernandez" />
            <Route path="/fheredia" />
          </Routes>
        </Layout.Content>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
